# Continuous Goal-Directed Actions

##About

This code repository contains the algorithms developed for my PhD thesis. It also contains the research papers generated and the slides used for presenting the PhD and the research in conferences. The thesis was developed in the [Robotics Lab research group](http://roboticslab.uc3m.es) at [Universidad Carlos III de Madrid](http://www.uc3m.es).

The PhD defense video is available [online](http://www.dailymotion.com/video/x3w7qw0_defensa-tesis-doctoral-uc3m-santiago-morante_tech).

## License

Unless otherwise noted, all the code contained in this repository is open-source and licensed under the MIT License.

## Links

* [Robotics Lab profile](http://roboticslab.uc3m.es/roboticslab/people/s-morante)
* [Twitter](https://twitter.com/santimorante)
* [LinkedIn](https://es.linkedin.com/in/santimorante)
* [Dailymotion](http://www.dailymotion.com/santimorante)
* [Google Scholar](http://scholar.google.es/citations?user=7_fJykAAAAAJ&hl=en)
* [Coursera](https://www.coursera.org/user/i/86e993631015eb245c7401b6efee50b2)
* [Mendeley](https://www.mendeley.com/profiles/santiago-morante/)

