## Source Code

* In `python` folder, you can find the python version of the project (latest development). 
    * Library dependencies: `opencv`, `numpy`, `FANN` (Fast Artificial Neural Network Library. Only for `nn.py`)
  
  
* In `cpp` folder, you can find the C++ version of the project (faster but outdated).
    * Library dependencies: `opencv`, `YARP` (Yet Another Robot Platform)
  
    
* In `superseeded` folder, you can find old development files.
