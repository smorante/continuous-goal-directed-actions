# Connected images

-- Abandoned project ---

##About
Algorithms to connect movements between images. One is a standard feedforward fully connected artificial neural network. The other is a own motion network. We compare actual images or images variations (until we decide which method is the best).


### Organization
* In `cross-validation` folder you can find all the data related to the experiments.

* In `synthetic` folder there are several videos to be used during development as examples of synthetic video files.

* In `src` folder you can find the source code, both python and c++ versions.

* In `video-execution` folder you can find some videos showing the execution of the code.

