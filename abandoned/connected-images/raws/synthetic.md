## Synthetic

* In `editable` folder, you can find vectorial files to create synthetic images, and also some ``.png`` images generated. 

* In `videos` folder, you can find some examples of generated synthetic videos. One of the videos (``tatup.mp4``) is a reduced version of [this video](https://www.youtube.com/watch?v=TJJkgnH3Xyc)

