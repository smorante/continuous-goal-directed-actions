# objects_minoru.avi
* Recorded: mar 27 16:14:24 CET 2015 with https://github.com/jgvictores/syncedCameraRecorder
* commit d54dfbaa086b2cf74ad8ae0ebe7ddb5c5cfa8b2a
* using unchanged settings (600 s, 20 fps, xvid, sync threshold 0.010 s),
* using a Minoru camera.
* Description: 8 colored objects moved by human manipulation.

