## Cross Validation

* In `experiments` folder, you can find files containing the parameters, results and weights used in each experiment. 

* In `test` folder, you can find files used as test data.

* In `training` folder, you can find files used to train the networks.
