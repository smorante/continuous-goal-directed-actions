## Experiments

* ``plot_mse.rmd`` is used to plot the Mean Square Error. You have to open the file and manually introduce the path to the results file (e.g. `../results/motion_results_20.Mar.2015_02:35:37.csv`).

* To record video from two different cameras at the same time, we use our own library called `syncedCameraRecorder`, publicly available: [syncedCameraRecorder](https://github.com/jgvictores/syncedCameraRecorder). This library is intended for recording synced videos from different cameras. All cameras are accepted, even USB cheap ones!. This library may be very useful for developing stereo vision applications with non-expensive off-the-shelf cameras.

