## Experiments

* In `params` folder, you can find files containing the parameters used in each experiment. Files are date and time coded. Each file contains the string `motion` in the filename if the network used is our own developed network, or `nn` if a standard feedforward fully connected neural network is used instead.

* In `results` folder, you can find files containing the error (Mean Square Error) between the predicted and the actual image. Files are date and time coded. Each file contains the string `motion` in the filename if the network used is our own developed network, or `nn` if a standard feedforward fully connected neural network is used instead.

* In `weights` folder, you can find files containing the saved weight matrix for each experiment. Files are date and time coded. Each file contains the string `motion` in the filename if the network used is our own developed network, or `nn` if a standard feedforward fully connected neural network is used instead.

* In `scripts` folder, you can find scripts to analyse and plot data of your experiments.
