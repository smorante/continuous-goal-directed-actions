#!/usr/bin/env python

from __future__ import division

import itertools
from sklearn import mixture, metrics
from sklearn.cluster import DBSCAN
from scipy import linalg
from scipy.spatial import distance
import pylab as pl
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy.interpolate import Rbf, InterpolatedUnivariateSpline
# import csv
import numpy as np
import itertools
import mlpy.dtwcore as md
import yarp


# reading file
def generalization(kinectTrajectory):
    realDataMatrix = []
    x=list(kinectTrajectory)
    temp0=np.array(x).astype('float')
    # Get the range and rescale
    r = float(temp0[-1][1] - temp0[0][1])
    temp0[:,1] = map(lambda x: (x - temp0[0][1]) / r, temp0[:,1])
    #normalize
    temp0 /= np.max(np.abs(temp0), axis=0)
    realDataMatrix.append(temp0.tolist())

    #if: test all dimensions
    Xnoisy = np.vstack(realDataMatrix) # noisy dataset

    # Compute similarities
    D = distance.squareform(distance.pdist(Xnoisy))
    S = 1 - (D / np.max(D))

    # Compute DBSCAN
    db = DBSCAN(eps=0.01, min_samples=3, metric='cosine').fit(S)
    labels = db.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

    print 'Estimated number of clusters: %d' % n_clusters_

    # Plotting DBSCAN (but also outlier detection)
    core_samples = db.core_sample_indices_
    unique_labels = set(labels)
    preplot = pl.subplot(4, 1, 1)
    colors = pl.cm.Blues(np.linspace(0, 1, len(unique_labels)))

    X=[]
    for k, col in zip(unique_labels, colors):
        class_members = [index[0] for index in np.argwhere(labels == k)]
        cluster_core_samples = [index for index in core_samples
                                if labels[index] == k]
        for index in class_members:
            if index in core_samples and k != -1:
                markersize = 8
                X.append(Xnoisy[index])
                pl.plot(Xnoisy[index][0], Xnoisy[index][1],'o', markerfacecolor=col, markeredgecolor='k', markersize=markersize)
            else:
                markersize = 3
                pl.plot(Xnoisy[index][0], Xnoisy[index][1],'o', markerfacecolor='k', markeredgecolor='k', markersize=markersize)

    if not X:
        X=Xnoisy
    #pl.xticks(())
    #pl.yticks(())
    #pl.title('DBSCAN. Estimated clusters: %d' % n_clusters_, size=20)

    #assigning new clean dataset to variable X in numpy array
    X = np.array(X)

    # Initializing BIC parameters
    lowest_bic = np.infty
    bic = []

    # choose number of clusters to test
    if n_clusters_ <2:
        componentToTest=3
    else:
        componentToTest=2*n_clusters_

    print "Maximum components tested: ", componentToTest
    n_components_range = range(1, componentToTest+1)

    # this is a loop to test every component, choosing the lowest BIC at the end
    for n_components in n_components_range:
        # Fit a mixture of gaussians with EM
        gmm = mixture.GMM(n_components=n_components, covariance_type='full')
        gmm.fit(X)
        bic.append(gmm.bic(X))
        if bic[-1] < lowest_bic:
            lowest_bic = bic[-1]
            best_gmm = gmm

    # over loading if components = 1
    if len(best_gmm.means_)==1:
        best_gmm = mixture.GMM(n_components=2, covariance_type='full')
        best_gmm.fit(X)

    # array of BIC for the graphic table column
    bic = np.array(bic)

    # one tested all components, here we choose the best
    clf = best_gmm
    print "Best result: ", clf
    #print 'Means: ', np.round(clf.means_,4)

    ## Plot the BIC scores
    #bars = []
    #spl = pl.subplot(4, 1, 2)

    #xpos = np.array(n_components_range) - 0.1
    #bars.append(pl.bar(xpos, bic[0:len(n_components_range)], width=.2, color='c'))

    #pl.xticks(n_components_range, size=15)
    #pl.yticks(([bic.min() * 1.01 - .01 * bic.max(), bic.max()]), size=12)
    #pl.title('BIC Score', size=20)
    #spl.set_xlabel('Number of components', size=15)

    ## Plot the winner
    #splot = pl.subplot(4, 1, 3)
    #Y_ = clf.predict(X)
    #for i, (mean, covar) in enumerate(zip(clf.means_, clf.covars_)):
        #v, w = linalg.eigh(covar)
        #if not np.any(Y_ == i):
            #continue
        ##pl.scatter(X[Y_ == i, 0], X[Y_ == i, 1], 8, color='black')
        #pl.plot(X[Y_ == i, 0], X[Y_ == i, 1], 'o', markerfacecolor='black', markeredgecolor='k', markersize=5)

        ## Plot an ellipse to show the Gaussian component
        #angle = np.arctan2(w[0][1], w[0][0])
        #angle = 180 * angle / np.pi  # convert to degrees
        #v *= 4
        #ell = mpl.patches.Ellipse(mean, v[0], v[1], 180 + angle, color='b')
        #ell.set_clip_box(splot.bbox)
        #ell.set_alpha(.6)
        #splot.add_artist(ell)

    #pl.xticks(())
    #pl.yticks(())
    #pl.title('GMM-BIC. Components: ' + str(len(clf.means_)), size=20)

    ## saving centers
    #np.savetxt("generalized/"+actionName+"Generalized", clf.means_,  fmt='%.14e')

    return clf.means_

    # plot interpolation
    #meansX, meansY = zip(*clf.means_)

    #if len(meansX) > 1:
        #minimTime=min(meansX)
        #maximTime=max(meansX)

        #print minimTime, maximTime

        #xi = np.linspace(minimTime, maximTime, 10*len(meansX))
        #testrbf = Rbf(meansX, meansY, function='gaussian')
        #yi = testrbf(xi)

        #pl.subplot(4, 1, 4)
        #pl.plot(xi, yi, 'g')
        #pl.scatter(meansX, meansY,20, color='blue')
        #pl.xticks(())
        #pl.yticks(())
        #pl.title('RBF Interpolation', size=20)
        #pl.subplots_adjust(hspace=.8, bottom=0.05)
        #pl.show()

    #else:
        #pl.show()


def recognition(idealAction, realAction):

    vectorOne = idealAction
    vectorTwo = realAction

    ###########################################
    ########## DTW all dimensions #############
    ###########################################

    print 'Number of Dimensions (included time): ', vectorOne.shape[1]
    distList =[]
    for i in range(1, vectorOne.shape[1]):

        #scatter plot
        #splot = pl.subplot(3, 1, 1)
        #pl.plot(vectorOne[:,0], vectorOne[:,i], 'o', markerfacecolor='blue', markeredgecolor='k', markersize=5)
        #pl.plot(vectorTwo[:,0], vectorTwo[:,i], 'o', markerfacecolor='red', markeredgecolor='k', markersize=5)
        #pl.title('Dimension ' + str(i), size=20)

        #interpolation 1
        minimTime=min(vectorOne[:,0])
        maximTime=max(vectorOne[:,0])
        xi = np.linspace(minimTime, maximTime, 10*len(vectorOne[:,i]))
        testrbfQ = Rbf(vectorOne[:,0], vectorOne[:,i], function='gaussian')
        yi = testrbfQ(xi)

        #pl.subplot(3, 1, 2)
        #pl.plot(xi, yi, 'g')
        #pl.scatter(vectorOne[:,0], vectorOne[:,i],20, color='blue')
        #pl.xticks(())
        #pl.yticks(())
        #pl.title('RBF Generalized', size=20)

        #interpolation 2
        minimTimeQ=min(vectorTwo[:,0])
        maximTimeQ=max(vectorTwo[:,0])
        xiQ = np.linspace(minimTimeQ, maximTimeQ, 10*len(vectorTwo[:,i]))
        testrbfQ = Rbf(vectorTwo[:,0], vectorTwo[:,i], function='gaussian')
        yiQ = testrbfQ(xiQ)

        #pl.subplot(3, 1, 3)
        #pl.plot(xiQ, yiQ, 'g')
        #pl.scatter(vectorTwo[:,0], vectorTwo[:,i],20, color='blue')
        #pl.xticks(())
        #pl.yticks(())
        #pl.title('RBF Query', size=20)

        #pl.subplots_adjust(hspace=.8, bottom=0.05)
        #pl.show()

        #dtw distance
        dist, px, py, cost = md.dtw(yi, yiQ)
        distList.extend([dist])
        #print 'Distance (lower, better) for Dim. ', str(i), ' : ', dist

        # plot accumulated cost matrix
        #fig = plt.figure(1)
        #ax = fig.add_subplot(111)
        #plot1 = plt.imshow(cost.T, origin='lower', cmap=cm.gray, interpolation='nearest')
        #ax.plot(px, py, color='red', linewidth=3)
        #xlim = ax.set_xlim((-0.5, cost.shape[0]-0.5))
        #ylim = ax.set_ylim((-0.5, cost.shape[1]-0.5))
        #plt.title("Accumulated cost matrix", size=25)
        #plt.show()

    # Total distance
    print 'Total distance: ', np.sum(distList)
    return np.sum(distList)

def main():

    yarp.Network.init()
    if yarp.Network.checkNetwork() != True:
        print '[fail] found no yarp network (try running "yarpserver &"), bye!'
        quit()
    real_port = yarp.Port()

    real_port.open("/effects/rpc:i")
    idealVector = [[5.26280647004110e-01,9.40929985361643e-01,1.99297500848694e-01,3.59167318963552e-01,6.07351801924851e-01,9.64503446557720e-01,9.73027449684553e-01,9.37618597257573e-01,9.65534113203597e-01],[1.82080560442786e-01,7.55183729105444e-01,3.34083123135452e-01,-1.51110213369183e-01,9.18005029397110e-01,9.72424236868364e-01,9.81053504449404e-01,8.92315878809692e-01,9.41119356135572e-01],[8.42290612149974e-01,7.41397119623235e-01,-1.78377223384907e-01,-5.15499144049478e-01,9.53053606031923e-01,9.78738747089666e-01,9.81845516537062e-01,8.80162345679110e-01,9.50031486609381e-01]]
    idealVector = np.array(idealVector).astype('float')

    ########################################
    while True:
        kinectBottle = yarp.Bottle()
        real_port.read(kinectBottle, True) # blocking

        #print 'got ', kinectBottle.toString()
        kinectVector = []
        for external in range(1,kinectBottle.size()):
            internalVector = []
            internalBottle = yarp.Bottle()
            internalBottle.clear()
            internalBottle = kinectBottle.get(external).asList()
            for internal in range(0,internalBottle.size()):
                internalVector.append(internalBottle.get(internal).asDouble())
            kinectVector.append(internalVector)

        realButGeneralized = generalization(kinectVector)

        #response = yarp.Bottle()
        #response.addString("ok")
        #real_port.write(response)

    #    idealBottle = yarp.Bottle()
    #    real_port.read(idealBottle)
    #    print 'got ', idealBottle.toString()
    #    idealVector = []
    #    for elem in range(1,idealBottle.size()):
    #        idealVector.append(idealBottle.get(elem).asDouble())

        fitness = recognition(idealVector, realButGeneralized)
        print 'fitness: ', fitness

        response = yarp.Bottle()
        response.addDouble(fitness)
        real_port.reply(response)
    ########################################################
    real_port.close()

if __name__ == '__main__':
	main()
