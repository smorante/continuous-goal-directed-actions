// -*- mode:C++; tab-width:3; c-basic-offset:4; indent-tabs-mode:nil -*-

#include "FunctionMinEvalOp.hpp"
#include <sstream>
#include <string>
#include <vector>
#include <valarray>     // std::valarray

/************************************************************************/


//double target[10]={0, 10, 20, 30, 40, 50, 60, 70, 80, 100};
double target[17]={0, 6.25, 12.5, 18.75, 25, 31.25, 37.5
                   , 43.75, 50, 56.25, 62.5, 68.75, 75, 81.25, 87.5, 93.75, 100};

double FunctionMinEvalOp::getCustomFitness(vector <double> genPoints){

    const int rows=4; //setting wall parameters
    const int cols=4;
    float percentage;
    int sqAr [rows*cols] = { }; //setting number of changed square as cero

     // // reset square color
    for(int i=0; i<(rows*cols); i++){
            stringstream rr;
            rr << "square" << i;
            _wall->GetLink(rr.str())->GetGeometry(0)->SetDiffuseColor(RaveVector<float>(0.5, 0.5, 0.5));
            rr.str("");
        }

    for(int t=0;t<=*pIter;t++) {
            std::vector<dReal> dEncRaw(probot->GetDOF());  // NUM_MOTORS
            if (t<*pIter){
//                cout << "< t: " << t << " *pIter: " << *pIter << std::endl;
//                cout << "pF0: " << pFresults->operator [](t*3+0) << std::endl;
//                cout << "pF1: " << pFresults->operator [](t*3+1) << std::endl;
//                cout << "pF2: " << pFresults->operator [](t*3+2) << std::endl;

                dEncRaw[0+18] = -1*(pFresults->operator [](t*3+0))*M_PI/180.0;  // simple
                dEncRaw[1+18] = -1*(pFresults->operator [](t*3+1))*M_PI/180.0;  // simple
                dEncRaw[3+18] = -1*(pFresults->operator [](t*3+2))*M_PI/180.0;  // simple
            }
            else if (t==*pIter){
//                cout << "== t: " << t << " *pIter: " << *pIter << std::endl;
//                cout << "gp0: " << genPoints[0] << std::endl;
//                cout << "gp1: " << genPoints[1] << std::endl;
//                cout << "gp2: " << genPoints[2] << std::endl;

                dEncRaw[0+18] = -genPoints[0]*M_PI/180.0;  // simple
                dEncRaw[1+18] = -genPoints[1]*M_PI/180.0;  // simple
                dEncRaw[3+18] = -genPoints[2]*M_PI/180.0;  // simple
            }
            else{cerr << "ERROR IN pIter or t" << std::endl;}

            dEncRaw[4+18] = -45*M_PI/180.0;
            probot->SetJointValues(dEncRaw);
            pcontrol->SetDesired(dEncRaw); // This function "resets" physics
            while(!pcontrol->IsDone()) {
                boost::this_thread::sleep(boost::posix_time::milliseconds(1));
            }

            penv->StepSimulation(0.0001);  // StepSimulation must be given in seconds
            T_base_object = _objPtr->GetTransform();
            double T_base_object_x = T_base_object.trans.x;
            double T_base_object_y = T_base_object.trans.y;
            double T_base_object_z = T_base_object.trans.z;

            //change square color in function of dist (end-effector,square)
            for(int i=0; i<(rows*cols); i++){
                    stringstream ss;
                    ss << "square" << i;
                    Transform pos_square = _wall->GetLink(ss.str())->GetGeometry(0)->GetTransform();

                    double pos_square_x = pos_square.trans.x;
                    double pos_square_y = pos_square.trans.y;
                    double pos_square_z = pos_square.trans.z;
                    double dist = sqrt(pow(T_base_object_x-pos_square_x,2)
                                  + pow(T_base_object_y-pos_square_y,2)
                                  + pow(T_base_object_z-pos_square_z,2) );

                       if (dist < 0.13){
                      _wall->GetLink(ss.str())->GetGeometry(0)->SetDiffuseColor(RaveVector<float>(0.0, 0.0, 1.0));
                        sqAr[i]=1;
                    }
                    ss.str("");
            }
    } //cierre bucle trayectoria completa

    std::valarray<int> myvalarray (sqAr,rows*cols);
    percentage= ( (float)myvalarray.sum()/(rows*cols))*100;

  //  cout << " per: " << percentage << std::endl;

    // calculate fit /percentage of painted wall
    double fit = abs(percentage-target[*pIter]);
 //   cout  << " fit: " << fit << std::endl;
    return fit;
}

/************************************************************************/

void FunctionMinEvalOp::registerParameters(StateP state) {
	state->getRegistry()->registerEntry("function", (voidP) (new uint(1)), ECF::UINT);
}

/************************************************************************/

bool FunctionMinEvalOp::initialize(StateP state) {

	voidP sptr = state->getRegistry()->getEntry("function"); // get parameter value
    stringstream msg;
    _objPtr = penv->GetKinBody("object");
    _wall = penv->GetKinBody("wall");

    if(!_objPtr) {
        fprintf(stderr,"error: object \"object\" does not exist.\n");
    } else printf("sucess: object \"object\" exists.\n");

    if(!_wall) {
        fprintf(stderr,"error: object \"wall\" does not exist.\n");
    } else printf("sucess: object \"wall\" exists.\n");


    usleep(1.0 * 1000000.0);

	return true;
}

/************************************************************************/

FitnessP FunctionMinEvalOp::evaluate(IndividualP individual) {

	// evaluation creates a new fitness object using a smart pointer
	// in our case, we try to minimize the function value, so we use FitnessMin fitness (for minimization problems)
	FitnessP fitness (new FitnessMin);

	// we define FloatingPoint as the only genotype (in the configuration file)
	FloatingPoint::FloatingPoint* gen = (FloatingPoint::FloatingPoint*) individual->getGenotype().get();

	// we implement the fitness function 'as is', without any translation
	// the number of variables is read from the genotype itself (size of 'realValue' vactor)

    double value =0;
    value= getCustomFitness(gen->realValue);
	fitness->setValue(value);
	return fitness;
}

/************************************************************************/

