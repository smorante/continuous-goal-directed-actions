// -*- mode:C++; tab-width:4; c-basic-offset:4; indent-tabs-mode:nil -*-

#include "MotorPrimitives.hpp"

int main(int argc, char **argv) {

    MotorPrimitives mod;
    mod.init();
    return mod.run();
}

